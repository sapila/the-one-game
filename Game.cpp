//
// Created by Nikos Gkogktzilas on 29/07/2018.
//

#include "Game.h"

SDL_Window* m_pWindow;
SDL_Renderer* m_pRenderer;
bool m_bRunning;

SDL_Texture* m_pTexture; // the new SDL_Texture variable
SDL_Rect m_sourceRectangle; // the first rectangle
SDL_Rect m_destinationRectangle; // another rectangle

bool Game::init(const char* title, int xpos, int ypos, int width, int height, int fullscreen){
    int flag = 0;

    if (fullscreen) {
        flag = SDL_WINDOW_FULLSCREEN;
    }

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::cout << "window init fail\n";
        return false; // window init fail
    }

    std::cout << "SDL init success\n";
    // init the window
    m_pWindow = SDL_CreateWindow(title, xpos, ypos, width, height, flag);
    // raise window to top
//    SDL_RaiseWindow( m_pWindow ); TODO: might not be needing this

    if(!m_pWindow) {
        std::cout << "window init fail\n";
        return false; // window init fail
    }

    std::cout << "window creation success\n";
    m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);

    if(!m_pRenderer) {
        std::cout << "renderer init fail\n";
        return false; // renderer init fail
    }

    std::cout << "renderer creation success\n";
    SDL_SetRenderDrawColor(m_pRenderer,0,0,0,255);

    std::cout << "init success\n";
    m_bRunning = true; // everything inited successfully,

    // Initialise Assets
    SDL_Surface* pTempSurface = SDL_LoadBMP("../assets/demo/herogame/Human/human_regular_hair.bmp");

    if (!pTempSurface) {
        std::cout << "asset can not be loaded";
    }
    m_pTexture = SDL_CreateTextureFromSurface(m_pRenderer, pTempSurface);
    SDL_FreeSurface(pTempSurface);

    SDL_QueryTexture(m_pTexture, NULL, NULL, &m_sourceRectangle.w, &m_sourceRectangle.h);

    m_sourceRectangle.w = 20;
    m_sourceRectangle.h = 20;
    m_sourceRectangle.x = 20;
    m_sourceRectangle.y = 20;

    m_destinationRectangle.x = 10;
    m_destinationRectangle.y = 10;
    m_destinationRectangle.w = m_sourceRectangle.w;
    m_destinationRectangle.h = m_sourceRectangle.h;

    return true;
}

/**
 * Return running status of the game
 * @return
 */
bool Game::running()
{
    return m_bRunning;
}

/**
 *
 */
void Game::handleEvents()
{
    SDL_Event event;
    if(SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                m_bRunning = false;
                break;
            default:
                break;
        }
    }
}

void Game::update()
{
    m_sourceRectangle.x = 20 * int(((SDL_GetTicks() / 100) % 6));
}
void Game::render()
{
    SDL_RenderClear(m_pRenderer); // clear the renderer to the draw color

    SDL_RenderCopyEx(m_pRenderer, m_pTexture,
                     &m_sourceRectangle, &m_destinationRectangle,
                     0, 0, SDL_FLIP_HORIZONTAL);
   // SDL_RenderCopy(m_pRenderer, m_pTexture, &m_sourceRectangle, &m_destinationRectangle);

    SDL_RenderPresent(m_pRenderer); // draw to the screen
}

void Game::clean()
{
    std::cout << "cleaning game\n";
    SDL_DestroyWindow(m_pWindow);
    SDL_DestroyRenderer(m_pRenderer);
    SDL_Quit();
}