//
// Created by Nikos Gkogktzilas on 29/07/2018.
//
#include<SDL.h>
#include <SDL_image.h>
#include <iostream>

#ifndef THE_ONE_GAME_GAME_H
#define THE_ONE_GAME_GAME_H


class Game {

public:
    bool init(const char* title, int xpos, int ypos, int width, int height, int fullscreen);

    bool running();

    void render();

    void handleEvents();

    void clean();

    void update();
};


#endif //THE_ONE_GAME_GAME_H
